﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General information about this build is provided by the following set
// attribute set. Change the values of these attributes to change the details
// associated with the build.
[assembly: AssemblyTitle("Form WF_PrintSample")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Form WF_PrintSample")]
[assembly: AssemblyCopyright("Copyright ©  2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting the value to False for the ComVisible parameter makes the types in this assembly invisible
// for COM components. If you need to refer to the type in this assembly through
// COM, set the ComVisible attribute to TRUE for this type.
[assembly: ComVisible(false)]

// The following GUID is used to identify the type library if this project is visible to COM
[assembly: Guid("6828272a-7c25-4225-b255-dda6f12b25a7")]

// Build version information consists of the following four values:
//
// Primary version number
// Additional version number
// Build number
// Revision
//
// You can set all values or accept assembly and revision numbers by default
// using "*" as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
