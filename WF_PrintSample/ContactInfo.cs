﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Form_WF_PrintSample
{
    public class ContactInfo
    {
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Document_Name { get; set; }
        public string Printer_Name { get; set; }
        
    }
}
