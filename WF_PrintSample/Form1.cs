﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JGBPrintDLL;
using static JGBPrintDLL.RawPrinterHelper;

namespace Form_WF_PrintSample
{
    public partial class Form1 : Form
    {
        private const string defaultTemplateName = "LabelTemplate001";

        private const string defaultDocumentName = "My document";
        
        static IDictionary<string, string> templates = new Dictionary<string, string>()
        {
            {"LabelTemplate001", "This is an example of printing directly to a printer.\r\n Document {{Document_Name}}.\r\n Printer {{Printer_Name}}\r\n   Person {{First_Name}} {{Last_Name}} \r\n Ship To: {{Address}} {{City}} {{Country}}" }
        };

        public Form1()
        {
            InitializeComponent();
            this.textBoxTemplateName.Text = defaultTemplateName;
        }

        private void ButtonPrint_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.textBoxPrinter.Text))
            {

                try
                {
                    var strTemplate =  templates.ContainsKey(this.textBoxTemplateName.Text)? templates[this.textBoxTemplateName.Text]:null;
                    if (string.IsNullOrEmpty(strTemplate))
                    {
                        MessageBox.Show(No_valid_template);
                        return;
                    }

                    var contactInfo = new ContactInfo()
                    {
                        First_Name = this.textBoxFirstName.Text,
                        Last_Name = this.textBoxLastName.Text,
                        Country = this.textBoxCountry.Text,
                        City = this.textBoxCity.Text,
                        Address = this.textBoxAddress.Text,
                        Document_Name = defaultDocumentName,
                        Printer_Name = this.textBoxPrinter.Text,
                    };

                    var s = TemplateHelper.GenerateString(strTemplate, contactInfo);
                    // Send a printer-specific to the printer.

                    var result = RawPrinterHelper.SendStringToPrinter(contactInfo.Printer_Name, s,contactInfo.Document_Name);   
                    if(result)
                        MessageBox.Show("Success");
                    else
                        MessageBox.Show($"Error Code {RawPrinterHelper.ErrorCode}");
                }
                catch (Exception ex)
                {
                   MessageBox.Show(ex.Message);
                }
                
            }
            else
            {
                MessageBox.Show(Printer_field_is_empty);
            }
            
        }
    }
}
