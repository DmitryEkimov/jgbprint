﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing;
using System.IO;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace JGBPrintDLL
{
public class RawPrinterHelper
{
    // Structure and API declarions:
    [StructLayout(LayoutKind.Sequential, CharSet=CharSet.Ansi)]
    public class DOCINFOA
    {
       [MarshalAs(UnmanagedType.LPStr)] public string pDocName;
       [MarshalAs(UnmanagedType.LPStr)] public string pOutputFile;
       [MarshalAs(UnmanagedType.LPStr)] public string pDataType;
    }

    public struct PPRINTER_OPTIONS
    {
       public uint cbSize;
       public uint dwFlags;
    }

    public enum PRINTER_OPTION_FLAGS
    {
        PRINTER_OPTION_NO_CACHE,
        PRINTER_OPTION_CACHE,
        PRINTER_OPTION_CLIENT_CHANGE
    };


    [DllImport("winspool.Drv", EntryPoint="OpenPrinterA", SetLastError=true, CharSet=CharSet.Ansi, ExactSpelling=true, CallingConvention=CallingConvention.StdCall)]
    public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

    [DllImport("winspool.drv", EntryPoint = "OpenPrinter2W", CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
    public static extern bool OpenPrinter2([MarshalAs(UnmanagedType.LPWStr)] string  pPrinterName, out IntPtr hPrinter, IntPtr pd, [In, MarshalAs(UnmanagedType.LPStruct)] PPRINTER_OPTIONS pOptions);

    [DllImport("winspool.Drv", EntryPoint="ClosePrinter", SetLastError=true, ExactSpelling=true, CallingConvention=CallingConvention.StdCall)]
    public static extern bool ClosePrinter(IntPtr hPrinter);

    [DllImport("winspool.Drv", EntryPoint="StartDocPrinterA", SetLastError=true, CharSet=CharSet.Ansi, ExactSpelling=true, CallingConvention=CallingConvention.StdCall)]
    public static extern bool StartDocPrinter( IntPtr hPrinter, Int32 level,  [In, MarshalAs(UnmanagedType.LPStruct)] DOCINFOA di);

    [DllImport("winspool.Drv", EntryPoint="EndDocPrinter", SetLastError=true, ExactSpelling=true, CallingConvention=CallingConvention.StdCall)]
    public static extern bool EndDocPrinter(IntPtr hPrinter);

    [DllImport("winspool.Drv", EntryPoint="StartPagePrinter", SetLastError=true, ExactSpelling=true, CallingConvention=CallingConvention.StdCall)]
    public static extern bool StartPagePrinter(IntPtr hPrinter);

    [DllImport("winspool.Drv", EntryPoint="EndPagePrinter", SetLastError=true, ExactSpelling=true, CallingConvention=CallingConvention.StdCall)]
    public static extern bool EndPagePrinter(IntPtr hPrinter);

    [DllImport("winspool.Drv", EntryPoint="WritePrinter", SetLastError=true, ExactSpelling=true, CallingConvention=CallingConvention.StdCall)]
    public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten );

    [DllImport("winspool.drv", EntryPoint = "FlushPrinter", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
    public static extern bool FlushPrinter(IntPtr hPrinter, IntPtr pBuf, Int32 cbBuf, out Int32 pcWritten, Int32 cSleep);

        public static Int32 ErrorCode = 0;
    private static IntPtr emptyPrinter = new IntPtr(0);
    // SendBytesToPrinter()
    // When the function is given a printer name and an unmanaged array
    // of bytes, the function sends those bytes to the print queue.
    // Returns true on success, false on failure.
    public static bool SendBytesToPrinter( string szPrinterName, IntPtr pBytes, Int32 dwCount, string szDocName)
    {
        Int32    dwWritten = 0;
        IntPtr    hPrinter = emptyPrinter;
        DOCINFOA    di = new DOCINFOA();
        bool    bSuccess = false; // Assume failure unless you specifically succeed.

        di.pDocName = szDocName;
        di.pDataType = "RAW";//(Environment.OSVersion.Version.Major >= 6)?"XPS_PASS":

        // Open the printer.
        if( OpenPrinter( szPrinterName.Normalize(), out hPrinter, IntPtr.Zero))
        {
            // Start a document.
            if( StartDocPrinter(hPrinter, 1, di) )
            {
                // Start a page.
                if( StartPagePrinter(hPrinter) )
                {
                    // Write your bytes.
                    bSuccess = WritePrinter(hPrinter, pBytes, dwCount, out dwWritten);
                    FlushPrinter(hPrinter, pBytes, dwCount, out dwWritten,2);
                }
                EndPagePrinter(hPrinter);
            }
            EndDocPrinter(hPrinter);
        }
        if(hPrinter != emptyPrinter) 
            ClosePrinter(hPrinter);
        // If you did not succeed, GetLastError may give more information
        // about why not.
        if( bSuccess == false )
        {
                ErrorCode = Marshal.GetLastWin32Error();
        }
        if (dwWritten != dwCount)
            return false;
        return bSuccess;
    }

    public static bool SendFileToPrinter( string szPrinterName, string szFileName )
    {
        // Open the file.
        FileStream fs = new FileStream(szFileName, FileMode.Open);
        // Create a BinaryReader on the file.
        BinaryReader br = new BinaryReader(fs);
        // Dim an array of bytes big enough to hold the file's contents.
        Byte []bytes = new Byte[fs.Length];
        bool bSuccess = false;
        // Your unmanaged pointer.
        IntPtr pUnmanagedBytes = new IntPtr(0);
        int nLength;

        nLength = Convert.ToInt32(fs.Length);
        // Read the contents of the file into the array.
        bytes = br.ReadBytes( nLength );
        // Allocate some unmanaged memory for those bytes.
        pUnmanagedBytes = Marshal.AllocCoTaskMem(nLength);
        // Copy the managed byte array into the unmanaged array.
        Marshal.Copy(bytes, 0, pUnmanagedBytes, nLength);
        // Send the unmanaged bytes to the printer.
        bSuccess = SendBytesToPrinter(szPrinterName, pUnmanagedBytes, nLength, szFileName);
        // Free the unmanaged memory that you allocated earlier.
        Marshal.FreeCoTaskMem(pUnmanagedBytes);
        return bSuccess;
    }

    private static PrinterDefinitionObject[] PrinterDefinitionObjects = null;

    public static bool SendStringToPrinter( string szPrinterName, string szString, string szDocName="C#.NET RAW Document" )
    {
        if (string.IsNullOrEmpty(szPrinterName))
        {
            throw new Exception("Printer field is empty"); 
        }

        if (PrinterDefinitionObjects == null)
        {
            PrinterDefinitionObjects = RawPrinterHelper.GetPrinterDefinitionObjects();
        }

        var printer = PrinterDefinitionObjects.FirstOrDefault(pdo => pdo.Name.ToUpper() == szPrinterName.ToUpper() || pdo.Caption.ToUpper() == szPrinterName.ToUpper() || pdo.DeviceId.ToUpper() == szPrinterName.ToUpper() || pdo.SharePath.ToUpper() == szPrinterName.ToUpper());

        if (printer?.Name==null)
        {
            throw new Exception("Printer name is wrong");
        }

        IntPtr pBytes;
        Int32 dwCount;
        // How many characters are in the string?
        dwCount = szString.Length;
        // Assume that the printer is expecting ANSI text, and then convert
        // the string to ANSI text.
        pBytes = Marshal.StringToCoTaskMemAnsi(szString);
        // Send the converted ANSI string to the printer.
        var res = SendBytesToPrinter(printer.IsNetwork? printer.SharePath: printer.Name, pBytes, dwCount,szDocName);//
        Marshal.FreeCoTaskMem(pBytes);
        return res;
    }

    public static PrinterDefinitionObject[] GetPrinterDefinitionObjects()
    {
        IDictionary<string, PrinterDefinitionObject> PrinterDefinitionObjects = new Dictionary<string, PrinterDefinitionObject>();
        try
        {
            ManagementObjectSearcher printers =
            new ManagementObjectSearcher("root\\CIMV2",
            "SELECT * FROM Win32_Printer");

            foreach (ManagementObject queryObj in printers.Get())
            {
                var pdo = new PrinterDefinitionObject()
                {
                    Caption = queryObj["Caption"]?.ToString(),
                    Name = queryObj["Name"]?.ToString(),
                    DeviceId = queryObj["DeviceID"]?.ToString(),
                    IsNetwork = (bool) queryObj["Network"],
                    SharePath = (bool) queryObj["Network"]
                        ?  queryObj["ServerName"]?.ToString() + "\\" + queryObj["ShareName"]?.ToString():string.Empty,
                    PortName = queryObj["PortName"]?.ToString(),


                };

                pdo.FriendlyPortName = pdo.IsNetwork ? pdo.SharePath : pdo.PortName; 

                if(!PrinterDefinitionObjects.ContainsKey(pdo.PortName))
                    PrinterDefinitionObjects.Add( pdo.PortName, pdo);
            }

            ManagementObjectSearcher printerPorts =
            new ManagementObjectSearcher("root\\CIMV2",
            "SELECT * FROM Win32_TCPIPPrinterPort");

            foreach (ManagementObject queryObj in printerPorts.Get())
            {
                var portName = queryObj["Name"].ToString();
                if (!PrinterDefinitionObjects.ContainsKey(portName))
                    continue;

                var pdo = PrinterDefinitionObjects[portName];
                pdo.HostAddress = queryObj["HostAddress"]?.ToString();
                pdo.PortNumber = queryObj["PortNumber"]?.ToString();
                pdo.Protocol =  (Protocol)(int)(uint)queryObj["Protocol"];
                pdo.FriendlyPortName = "tcp://" + pdo.HostAddress + ":" + pdo.PortNumber;
            }

            
        }
        catch (ManagementException e)
        {
            
        }

        return PrinterDefinitionObjects.Values.ToArray();
    }

    public class PrinterDefinitionObject
    {
        public string Caption { get; set; }
        public string Name { get; set; }
        public string DeviceId { get; set; }
        public bool IsNetwork  { get; set; }
        public string SharePath { get; set; }
        public string PortName  { get; set; }
        public string HostAddress { get; set; }
        public string PortNumber  { get; set; }
        public Protocol Protocol { get; set; }
        public string FriendlyPortName { get; set; }
        
    }

    public enum Protocol { SAMBA=0, TCPIP=1 }
  }
}
