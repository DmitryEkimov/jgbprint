﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JGBPrintDLL
{
    public static class TemplateHelper
    {
        public static string GenerateString<T>(string template, T contact)
        {
            if (contact == null)
                return string.Empty;
            var typeInfo = contact.GetType().GetTypeInfo();
            var properties = typeInfo.DeclaredProperties.Where(p => p.CanRead)
                .Select(p => new KeyValuePair<string,string>(p.Name,p.GetValue(contact)?.ToString()));

            /// https://docs.microsoft.com/ru-ru/dotnet/api/system.string.copy
            var result = (new StringBuilder(template)).ToString();
            foreach (var property in properties)
            {
                template = template.Replace("{{"+property.Key+"}}", property.Value);
            }
            return template;
        }
    }
}
